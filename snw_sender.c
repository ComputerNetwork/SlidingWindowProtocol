#include "common.h"

//Socket FD.
static int sockfd;
static int sockfd2;

static int MODE;
static char PORT[PLEN];
static char HOSTNAME[HNLEN];
static char FILENAME[FNLEN];

char buff[BUFFSIZE];
char last_packet[BUFFSIZE];
int lPacketLen;
#define RCVTO 1
#define MYHOST "localhost"
#define MYPORT "4860"
struct addrinfo *raddr;
struct sockaddr_storage *other;
socklen_t addrlen;

void *get_in_addr(struct sockaddr *sa)
{
	return &(((struct sockaddr_in*)sa)->sin_addr);
}
void usage(){
	printf("./SNW_Sender -p 4859 -h localhost -f sample.txt -m 1\n");
	exit(EXIT_FAILURE);
}
int open_udp_socket(){

	struct addrinfo hints, *servinfo;
	struct addrinfo *p;
	int rv;

	
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET; 
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = AI_PASSIVE; 
	//Bind to local host. receiver connects to (MYHOST:MYPORT)
	if ((rv = getaddrinfo(MYHOST, MYPORT, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		exit(EXIT_FAILURE);
	}

	// loop through all the results and bind to the first we can
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype,
			p->ai_protocol)) == -1) {
				perror("snw_receiver: socket");
				continue;
		}
		if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			perror("SNW_receiver: bind");
			continue;
		}
		break;
	}

	if (p == NULL) {
		fprintf(stderr, "snw_receiver: failed to open socket\n");
		exit(EXIT_FAILURE);
	}
	freeaddrinfo(servinfo);
	
	//Populate receiver Addr into raddr
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET; 
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = AI_PASSIVE; 

	if ((rv = getaddrinfo(HOSTNAME, PORT, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		exit(EXIT_FAILURE);
	}

	// loop through all the results and connect to the first we can
	for(raddr = servinfo; raddr != NULL; raddr = raddr->ai_next) {
		if ((sockfd2 = socket(raddr->ai_family, raddr->ai_socktype,
								raddr->ai_protocol)) == -1) {
				perror("SNW_Sender: socket");
				continue;
		}
		break;
	}
	if(raddr == NULL){
		fprintf(stderr, "snw_receiver: failed to open socket\n");
		exit(EXIT_FAILURE);
	}

}

/*
 * Parses arg list and loads values
 * Args:
 *  argc    - Argument count
 *  argv    - Array of char array (Argument list)
 * Returns:
 *  None
 * */
void parse_and_validate_args(int argc, char **argv){
	int c;
    bool mFlag = false, pFlag = false, hFlag = false, fFlag = false;
    int port = 0;
    while((c = getopt(argc, argv, "p:f:h:m:")) != -1){
        switch(c){
            case 'm':
                MODE = strtol(optarg, NULL, 10);
                mFlag = true;
                break;
            case 'p':
                if(strlen(optarg) > PLEN){
                	printf("Port length too long\n");
                	exit(EXIT_FAILURE);
                }
                strcpy(PORT, optarg);
                port = strtol(optarg, NULL, 10);
                pFlag = true;
                break;
            case 'f':
                if(strlen(optarg) > FNLEN){
                	printf("FILENAME length too long\n");
                	exit(EXIT_FAILURE);
                }
                strcpy(FILENAME, optarg);
                fFlag = true;
                break;
            case 'h':
                if(strlen(optarg) > HNLEN){
                	printf("HOSTNAME length too long\n");
                	exit(EXIT_FAILURE);
                }
                strcpy(HOSTNAME, optarg);
                hFlag = true;
                break;
            default:  
                usage();
        }
    }
    if(!mFlag || MODE != 1){
		printf("MODE value is mandatory. Must be a 1\n");
        usage();
    } 
    if(!pFlag || port <= 0 || port > MAX_PORTS){
		printf("PORT value is mandatory. Must be within 0-65535\n");
        usage();
    } 
    if(!fFlag || (strcmp(FILENAME, "") == 0)){
		printf("FILENAME is mandatory\n");
        usage();    	
    }
    if(!hFlag || (strcmp(HOSTNAME, "") == 0)){
		printf("HOSTNAME is mandatory\n");
        usage();    	
    }
}
void print_args(){
	printf("ARGS:\n\nMODE: %d\nPORT: %s\nFILE:%s\nHOST:%s\n",
			MODE, PORT, FILENAME, HOSTNAME);
}

int compose_data_packet(int fd, int seqNum){
	int numbytes = 0;
	char file_buff[BUFFSIZE];
	UFTP_Header_t header;
	header.type = DATA;
	header.mode = 1;
	header.seq = seqNum;
	header.ack = 0;
	numbytes = sizeof(header);
	size_t len;
	len = read(fd, file_buff, BUFFSIZE-numbytes-1);
	if(len < 0){
		perror("SNW_Sender: Read");
		exit(EXIT_FAILURE);
	}
	header.len = len;
	memcpy(buff, &header, numbytes);
	if(len == 0){
		return numbytes;
	}
	memcpy(buff+numbytes, file_buff, len);
	numbytes += len;
	return numbytes;
}

int process_init_packet(char *buff, int numbytes){
	UFTP_Header_t *header = (UFTP_Header_t *)buff;
	#ifdef DBG
	printf("INIT:>SN: %d ACK:%d MODE:%d Type:%d\n", \
			header->seq, header->ack, header->mode, header->type);
	#endif
	if(header->mode != 1 || header->type != INIT)
		return 0;
	return 1;
}

int process_ack_packet(char *buff, int numbytes, int seqNum){
	UFTP_Header_t *header = (UFTP_Header_t *)buff;
	#ifdef DBG
	printf("ACK:>SN: %d ACK:%d MODE:%d Type:%d\n", \
			header->seq, header->ack, header->mode, header->type);
	#endif
	if(header->mode != 1 || header->type != ACK)
		return 0;
	if(header->ack != (seqNum+1)%MAX_SEQNUM)
		return 0;
	return header->ack;

}
size_t get_file_length(){
	int fd = open(FILENAME, O_RDONLY);
	if(fd == -1){
		perror("snw_sender: open");
		exit(EXIT_FAILURE);
	}
	size_t len = lseek(fd, 0, SEEK_END);
	if(len <= 0){
		perror("snw_sender: lseek");
		exit(EXIT_FAILURE);
	}
	#ifdef DBG
	printf("snw_sender: FileLength: %ld\n", len);
	#endif
	close(fd);
	return len;
}

void initialize_connection(off_t *f_len, int *seqNum){
	size_t numbytes;
	struct timeval tv;
	tv.tv_sec = RCVTO;
	tv.tv_usec = 0;
	if(setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) == -1){
		perror("SNW_Sender: settimeout");
		exit(EXIT_FAILURE);
	}
	memset(buff, 0, BUFFSIZE);
	int rc = 0;
	//Compose INIT message
	*f_len = get_file_length();
	while(rc < MAX_RETRY_COUNT){
		UFTP_Header_t header = {INIT, 1, *seqNum, 0, 0};
		memcpy(buff, &header, sizeof(header));	
		int n = snprintf(buff+sizeof(header), BUFFSIZE, "%s\t%ld",\
							 FILENAME, *f_len);
		if(n <= 0){
			perror("SNW_Sender: snprintf");
			exit(EXIT_FAILURE);
		}
		if ((numbytes = sendto(sockfd2, buff, n+sizeof(header), 0,
								raddr->ai_addr, raddr->ai_addrlen)) == -1) {
			perror("SNW_Sender: sendto");
			exit(1);
		}
		memset(buff, 0, BUFFSIZE);
		//Receive ACK
		addrlen = sizeof(struct sockaddr_storage);
		if ((numbytes = recvfrom(sockfd, buff, BUFFSIZE-1 , 0,
					(struct sockaddr *)other, &addrlen)) == -1) {
				if(errno == EAGAIN || errno == EWOULDBLOCK){
					rc++;
					printf("Retrying..%d\n", rc);
					continue;
				}	
				perror("recvfrom");
				exit(1);
		}
		int ack = process_ack_packet(buff, numbytes, *seqNum);
		if(ack != 0){
			*seqNum = ack;
			break;
		}
		memset(buff, 0, BUFFSIZE);
	}
	if(rc >= MAX_RETRY_COUNT){
		printf("Error receiving INITACK.\n");
		exit(EXIT_FAILURE);
	}
}

void handle_receiver(){
	int numbytes, rv, dataLen;	
	char s[INET_ADDRSTRLEN];
	time_t t;
	//INITIALIZE seq Number
	srand((unsigned) time(&t));
	int seqNum = 1+(rand()%100);
	off_t offset = 0;
	size_t f_len = 0;
	
	initialize_connection(&f_len, &seqNum);
	
	int retryCount = 0;
	bool isRetry = false;
	bool isDuplicateAck = false;
	int ii = 0;
	int fd = open(FILENAME, O_RDONLY);
	if(!fd){
		perror("SNW_Sender: Open");
		exit(EXIT_FAILURE);
	}
	while(retryCount <= MAX_RETRY_COUNT && f_len > offset){
		if(!isRetry && !isDuplicateAck){
			//SEND DATA message
			dataLen = compose_data_packet(fd, seqNum);
			if(dataLen <= 0){
				printf("Error composing Data packet\n");
				exit(EXIT_FAILURE);
			}		
			offset += (dataLen - sizeof(UFTP_Header_t));	
			//Backup the last message for retransmission.	
			memcpy(last_packet, buff, dataLen);
		}
		else
			memcpy(buff, last_packet, dataLen);		
		#ifdef DBG
		if(ii == 5){
			UFTP_Header_t *h = (UFTP_Header_t*)buff;
			printf("Sleeping %d\n", h->seq);
			ii++;	
			sleep(2);
		}
		else{
		#endif
			if ((rv = sendto(sockfd2, buff, dataLen, 0,
								raddr->ai_addr, raddr->ai_addrlen)) == -1) {
				perror("SNW_Sender: sendto");
				exit(1);
			}
			memset(buff, 0, BUFFSIZE);
		#ifdef DBG
		}
		ii++;	
		#endif
		//RECV ACK
		addrlen = sizeof(struct sockaddr_storage);
		if ((numbytes = recvfrom(sockfd, buff, BUFFSIZE-1 , 0,
				(struct sockaddr *)other, &addrlen)) == -1) {
				if(errno == EAGAIN || errno == EWOULDBLOCK){
					retryCount++;
					isRetry = true;
					UFTP_Header_t *h = (UFTP_Header_t*)last_packet;
					#ifdef DBG
					printf("Retrying %d %d\n", retryCount, h->seq);
					#endif
					continue;
				}				
				perror("recvfrom");
				exit(1);
		}
		isRetry = false;
		retryCount = 0;
		
		int ack = process_ack_packet(buff, numbytes, seqNum);
		if(ack != 0){
			seqNum = ack;
			isDuplicateAck = false;
		}
		else{
			#ifdef DBG
			printf("snw_sender: Duplicate Ack\n");
			#endif
			isDuplicateAck = true;
		}
		memset(buff, 0, BUFFSIZE);
	}
	if(retryCount >= MAX_RETRY_COUNT){
		printf("Retry Count exceeded. Terminating Connection...\n");
		exit(EXIT_FAILURE);
	}
	else{
		printf("snw_sender: File Transfer Completed\n");
	}

}

int main(int argc, char **argv)
{

	parse_and_validate_args(argc, argv);
	#ifdef DBG
	print_args();
	#endif
	open_udp_socket();
	
	handle_receiver();
	close(sockfd);
	close(sockfd2);
	
	return 0;
}
