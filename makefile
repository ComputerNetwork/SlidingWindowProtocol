snw_sender: snw_sender.c
	gcc -g snw_sender.c -o snw_sender

snw_receiver: snw_receiver.c
	gcc -g snw_receiver.c -o snw_receiver

gbn_sender: gbn_sender.c buffer_list.c
	gcc -g gbn_sender.c buffer_list.c -o gbn_sender

gbn_receiver: gbn_receiver.c
	gcc -g gbn_receiver.c -o gbn_receiver

sr_sender: sr_sender.c srs_buffer_list.c
	gcc -g sr_sender.c srs_buffer_list.c -o sr_sender

sr_receiver: sr_receiver.c src_buffer_list.c
	gcc -g sr_receiver.c src_buffer_list.c -o sr_receiver
	
snws: snw_sender
	./snw_sender -p 4859 -h localhost -f hw3.tar.gz -m 1

snwc: snw_receiver
	./snw_receiver -p 4860 -h localhost -m 1

gbns: gbn_sender
	./gbn_sender -p 4859 -h localhost -f hw3.tar.gz -m 5

gbnc: gbn_receiver
	./gbn_receiver -p 4860 -h localhost -m 5	

srs: sr_sender
	./sr_sender -p 4859 -h localhost -f sample.txt -m 5

src: sr_receiver
	./sr_receiver -p 4860 -h localhost -m 5	

	
clean: 
	rm -rf ./snw_sender ./gbn_sender ./gbn_receiver ./snw_receiver ./sr_receiver ./sr_sender ./core

default: claunch 
