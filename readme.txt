File organization:
-------------------
common.h - Common DS for all the implementations.

snw_sender.c, snw_receiver.c - Stop-And-Wait sender and receiver.
gbn_sender.c, gbn_receiver.c - Go-Back-N sender and receiver.
sr_sender.c, sr_receiver.c   - Selective Repeat sender and receiver.

buffer_list.c - Helper for Go-Back-N sender to maintain buffer of size MODE.

srs_buffer_list.c - Helper for Selective Repeat sender to maintain buffer of
size MODE.
src_buffer_list.c - Helper for SR receiver (In SR, receiver also maintains
a Window of size MODE)

Configuration:
---------------
- Place all the files in the folder.

- Sender & Receiver will bind to different address & port. This address must be given as
  -h and -p option to the other end using the CMD line args. If there is any
  mistake it will fail.
 
- By default, sender and receiver binds to local host at ports 4859, 4860 resp. 
  If you want to change it, update them inside sender.c and receiver.c file.
- If you are running the sender in host A,(ex. a.ccs.neu.edu you have to insert
  update the values in MYHOST and MYPORT inside the sender.c file.
- Similarly update this for receiver. 

- In Make file, you can change the arguments you are passing to run the code.

Tests:
------
- Normal testing done at tam.ccs and top.ccs machines. With gz file, png file and
txt file. All worked fine.

To test this code, I have included sleep, and sometimes skip sending data/acks etc. 
See README.md for test cases.

(I couldn't install wireshark/tcpdump in ccs machine.So tested locally.)

Test1(Drop/delayed ACK SNW): See test1.png you can find data packet is retransmitted after the timeout.
Test2(Drop DATA): See test2.png where there will be a long wait (1.0x - 4.x)
during which receiver was retrying and sender also timedout waiting for ACK.
After that data is transmitted.

Test3(Drop/Delayed ACK GBN): Tested with MODE = 5, you can see 5 packets getting
retransmitted after timeout.
Test4(Drop Data): See test4.png, Packet 3 is not sent and the retransmission
 of packets 3,4,5 happens as expected.

Test 5: Drop Data: See test5.png: Packet 3 is dropped by sender. Receiver sends
NACK and only that packet is retransmitted.


