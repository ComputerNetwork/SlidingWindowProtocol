#include "common.h"
#include "buffer_list.h"
node *head = NULL;
node *tail = NULL;

static node* create_node(int seq, char *data, int size){
	node *tmp = (node *)malloc(sizeof(node));
	if(!tmp){
		perror("Create node\n");
		exit(EXIT_FAILURE);
	}
	tmp->seq = seq;
	memcpy(tmp->data, data, size);
	tmp->next = NULL;
	tmp->size = size;
	return tmp;
}

void insert(int seq, char *data, int size){
	node *tmp = create_node(seq, data, size);
	if(head == NULL){
		head = tmp;
		tail = tmp;
	}
	else{
		tail->next = tmp;
		tail = tmp;		
	}
	#ifdef DBG
	printf("Inserted %d %p\t%p\n", tmp->seq, tmp, head);
	#endif
}

void delete_node(){
	if(head == NULL || tail == NULL){
		return;
	}
	node *tmp = head;
	head = head->next;
	free(tmp);
	tmp = NULL;
}

void print_list(){
	node* tmp = head;
	while(tmp){
		printf("%d(%d)->", tmp->seq, tmp->size);
		tmp = tmp->next;
	}
	printf("\n");
}

