#ifndef COMMON_H_
#define COMMON_H_

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <fcntl.h>
#include<time.h>

//Enable this flag for debugging...
//#define DBG

#define HNLEN 100
#define FNLEN 1000
#define PLEN 6
#define MAX_SEQNUM 65535
#define MAX_RETRY_COUNT 5

#define MAX_PORTS 65535

#define BUFFSIZE 1500
#define MAX_MODE 100

typedef enum ptype{
	INIT,
	DATA,
	ACK
}ptype;

//UDP File Transfer Protocol Header
typedef struct UFTP_Header_t{
	//Packet Type. (0: INIT, 1: ACK, 2: DATA)
	ptype type:16;
	//Mode - Denote protocol (Stop-And-Wait/Go-Back-N/Selective Repeat)
	int mode:16;
	//Sequence Number
	int seq:16;
	//Acknowledgement Number
	int ack:16;
	//Length of payload
	size_t len;	
}UFTP_Header_t;


#endif
