#include "common.h"

//Socket FD.
static int sockfd;
static int sockfd2;

static int MODE;
static char PORT[PLEN];
static char HOSTNAME[HNLEN];
static char FILENAME[FNLEN];

char buff[BUFFSIZE];
static size_t LENGTH;	
#define RCVTO 1
#define MYHOST "localhost"
#define MYPORT "4859"
//Sender addr info
struct addrinfo *saddr;
struct sockaddr_storage *other;
socklen_t addrlen;

void *get_in_addr(struct sockaddr *sa)
{
	return &(((struct sockaddr_in*)sa)->sin_addr);
}
void usage(){
	printf("./snw_receiver -p 4859 -h localhost -m 1\n");
	exit(EXIT_FAILURE);
}
int open_udp_socket(){

	struct addrinfo hints, *servinfo;
	struct addrinfo *p;
	int rv;

	
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET; 
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = AI_PASSIVE; 
	//Bind to local host. Sender connects to (MYHOST:MYPORT)
	if ((rv = getaddrinfo(MYHOST, MYPORT, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		exit(EXIT_FAILURE);
	}

	// loop through all the results and bind to the first we can
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype,
			p->ai_protocol)) == -1) {
				perror("snw_receiver: socket");
				continue;
		}
		if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			perror("SNW_receiver: bind");
			continue;
		}
		break;
	}

	if (p == NULL) {
		fprintf(stderr, "snw_receiver: failed to open socket\n");
		exit(EXIT_FAILURE);
	}
	freeaddrinfo(servinfo);
	
	//Populate Sender Addr into saddr
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET; 
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = AI_PASSIVE; 

	if ((rv = getaddrinfo(HOSTNAME, PORT, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		exit(EXIT_FAILURE);
	}

	// loop through all the results and connect to the first we can
	for(saddr = servinfo; saddr != NULL; saddr = saddr->ai_next) {
		if ((sockfd2 = socket(saddr->ai_family, saddr->ai_socktype,
			saddr->ai_protocol)) == -1) {
				perror("SNW_Sender: socket");
				continue;
		}
		break;
	}
	if(saddr == NULL){
		fprintf(stderr, "snw_receiver: failed to open socket\n");
		exit(EXIT_FAILURE);
	}

}

/*
 * Parses arg list and loads values
 * Args:
 *  argc    - Argument count
 *  argv    - Array of char array (Argument list)
 * Returns:
 *  None
 * */
void parse_and_validate_args(int argc, char **argv){
	int c;
    bool mFlag = false, pFlag = false, hFlag = false;
    int port = 0;
    while((c = getopt(argc, argv, "p:h:m:")) != -1){
        switch(c){
            case 'm':
                MODE = strtol(optarg, NULL, 10);
                mFlag = true;
                break;
            case 'p':
                if(strlen(optarg) > PLEN){
                	printf("Port length too long\n");
                	exit(EXIT_FAILURE);
                }
                strcpy(PORT, optarg);
                port = strtol(optarg, NULL, 10);
                pFlag = true;
                break;
            case 'h':
                if(strlen(optarg) > HNLEN){
                	printf("HOSTNAME length too long\n");
                	exit(EXIT_FAILURE);
                }
                strcpy(HOSTNAME, optarg);
                hFlag = true;
                break;
            default:  
                usage();
        }
    }
    if(!mFlag || MODE != 1){
		printf("MODE value is mandatory. Must be a 1\n");
        usage();
    } 
    if(!pFlag || port <= 0 || port > MAX_PORTS){
		printf("PORT value is mandatory. Must be within 0-65535\n");
        usage();
    }
    if(!hFlag || (strcmp(HOSTNAME, "") == 0)){
		printf("HOSTNAME is mandatory\n");
        usage();    	
    }
}
void print_args(){
	printf("ARGS:\n\nMODE: %d\nPORT: %s\nHOST:%s\n",
			MODE, PORT, HOSTNAME);
}

int process_init_packet(char *buff, int numbytes){
	UFTP_Header_t *header = (UFTP_Header_t *)buff;
	#ifdef DBG
	printf("SN: %d ACK:%d MODE:%d Type:%d\n", \
			header->seq, header->ack, header->mode, header->type);
	#endif
	if(header->mode != 1 || header->type != INIT)
		return 0;
	char fileName[BUFFSIZE];
	sscanf(buff+sizeof(UFTP_Header_t), "%s\t%ld", fileName, &LENGTH);
	int n = snprintf(FILENAME, FNLEN, "%d_%s", getpid(), fileName);
	if(n <= 0){
		printf("SNW_receiver: snprintf failed");
		return 0;
	}
	printf("File: %s and Length: %ld\n", FILENAME, LENGTH);
	return header->seq;
}

int process_data_packet(char *buff, int ackNum, bool firstPacket){
	UFTP_Header_t *header = (UFTP_Header_t *)buff;
	int fd;
	#ifdef DBG
	printf("Data Packet:> Type: %d SEQ:%d ACK:%d len:%ld\n",\
		header->type, header->seq, ackNum, header->len);
	#endif
	if(header->type != DATA || header->len <= 0){
		printf("Data Packet Mismatch:> Type: %d SEQ:%d ACK:%d len:%ld\n",\
				header->type, header->seq, ackNum, header->len);
		return -2;
	}
	if(header->seq != ackNum){
		return 0;
	}	
	
	//Read data and write to file	
	if(firstPacket)
		fd = open(FILENAME, O_CREAT | O_WRONLY, 0666);	
	else
		fd = open(FILENAME, O_WRONLY | O_APPEND);
		
	if(fd == -1){
		perror("snw_receiver: open");
		exit(EXIT_FAILURE);
	}
	int n = write(fd, buff+sizeof(UFTP_Header_t), header->len);
	if(n <= 0){
		perror("snw_receiver: write");
		exit(EXIT_FAILURE);
	}	
	close(fd);	
	LENGTH -= header->len;
	return header->seq;
	
}

int compose_ack_packet(char *buff, int ack, int seq){
	UFTP_Header_t header = {ACK, 1, seq, ack, 0};	
	memcpy(buff, &header, sizeof(header));
	return sizeof(UFTP_Header_t);
}

void initialize_connection(int *seqNum, int *ackNum, int *lAckSize,\
								char *last_ack){	

	size_t numbytes;
	addrlen = sizeof(struct sockaddr_storage);
	//Receive INIT Message
	if ((numbytes = recvfrom(sockfd, buff, BUFFSIZE-1 , 0,
				(struct sockaddr *)other, &addrlen)) == -1) {
			perror("recvfrom");
			exit(1);
	}
	struct timeval tv;
	tv.tv_sec = RCVTO;
	tv.tv_usec = 0;
	if(setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) == -1){
		perror("SNW_Receiver: settimeout");
		exit(EXIT_FAILURE);
	}
	*seqNum = process_init_packet(buff, numbytes);
	if(!*seqNum){
		printf("snw_receiver: Init packet not proper\n");
		exit(EXIT_FAILURE);
	}
	memset(buff, 0, BUFFSIZE);

	//Send ACK
	*ackNum = (*seqNum+1)%MAX_SEQNUM;	
	numbytes = compose_ack_packet(buff, *ackNum, *seqNum);
	memcpy(last_ack, buff, numbytes);	
	*lAckSize = numbytes;	
	if ((numbytes = sendto(sockfd2, buff, numbytes, 0,
					saddr->ai_addr, saddr->ai_addrlen)) == -1) {
		perror("snw_receiver: sendto");
		exit(1);
	}
	memset(buff, 0, BUFFSIZE);	
}

int main(int argc, char **argv)
{
	int numbytes;	
	parse_and_validate_args(argc, argv);
	#ifdef DBG
	print_args();
	#endif
	open_udp_socket();
	int ackNum, lAckSize = 0, seqNum;
	char last_ack[BUFFSIZE];
	initialize_connection(&seqNum, &ackNum, &lAckSize, last_ack);										
	
	bool firstPacket = true;
	int ii = 0;

	int retryCount = 0;
	while(LENGTH > 0 && retryCount < MAX_RETRY_COUNT){
		//Receive DATA Message
		addrlen = sizeof(struct sockaddr_storage);
		if ((numbytes = recvfrom(sockfd, buff, BUFFSIZE-1 , 0,
					(struct sockaddr *)other, &addrlen)) == -1) {
			if(errno == EAGAIN || errno == EWOULDBLOCK){
				retryCount++;
				#ifdef DBG
				printf("Retrying...\n");
				#endif
				continue;			
			}
			perror("recvfrom");
			exit(1);
		}
		retryCount = 0;
		#ifdef DBG
		printf("Received %d bytes\n", numbytes);
		#endif
		seqNum = process_data_packet(buff, ackNum, firstPacket);
		if(seqNum == -2){
			printf("snw_receiver: Data packet not proper\n");
			exit(EXIT_FAILURE);		
		}
		else if(seqNum == 0){
			#ifdef DBG
			printf("snw_receiver: Duplicate Packet\n");
			//Ack is not updated. Send the old ACK asking Retransmission
			#endif
		}		
		else{
			ackNum = (seqNum+1)%MAX_SEQNUM;
			firstPacket = false;
		}
			
		memset(buff, 0, BUFFSIZE);
			
		//Send ACK message		
		numbytes = compose_ack_packet(buff, ackNum, seqNum);
		memcpy(last_ack, buff, numbytes);	
		lAckSize = numbytes;
		#ifdef DBG	
		if(ii == 3){
			ii++;
			continue;
		}
		#endif
		if ((numbytes = sendto(sockfd2, buff, numbytes, 0,
						saddr->ai_addr, saddr->ai_addrlen)) == -1) {
			perror("snw_receiver: sendto");
			exit(1);
		}
		memset(buff, 0, BUFFSIZE);
		ii++;	
	}
	if(LENGTH != 0){
		printf("snw_receiver: Error. File transfer not completed...\n");
		exit(EXIT_FAILURE);
	}
	else if(retryCount >= MAX_RETRY_COUNT){
		printf("Max retry exceeded..\n");
		exit(EXIT_FAILURE);		
	}
	else{
		printf("snw_receiver: File transfer completed...\n");
	}
	close(sockfd);
	close(sockfd2);
	return 0;
}
