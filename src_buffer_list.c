#include "common.h"
#include "srs_buffer_list.h"
node *head = NULL;
node *tail = NULL;

static node* create_node(int seq, char *data, int size){
	node *tmp = (node *)malloc(sizeof(node));
	if(!tmp){
		perror("Create node\n");
		exit(EXIT_FAILURE);
	}
	tmp->seq = seq;
	memcpy(tmp->data, data, size);
	tmp->next = NULL;
	tmp->size = size;
	return tmp;
}

void insert(int seq, char *data, int size){
	node *tmp = create_node(seq, data, size);
	node *start = head;
	node *prev = head;
	bool done = false;
	
	if(head == NULL){
		head = tmp;
	}
	else{
		while(start){
			if(start->seq > tmp->seq){
				
				if(start == prev){
					tmp->next = start;
					head = tmp;
				}
				
				else{
					tmp->next = start;
					prev->next = tmp;
				}				
				done = true;			
			}
			prev = start;
			start = start->next;
		}
		if(!done){
			prev->next = tmp;
		}	
	}
}

void delete_node(){
	if(head == NULL || tail == NULL){
		return;
	}
	node *tmp = head;
	head = head->next;
	free(tmp);
	tmp = NULL;
}

void empty_list(){
	while(head){
		node *tmp = head;
		head = head->next;
		free(tmp);
		tmp = NULL;
	}
	head = NULL;
}

int get_missing(){
	if(!head)
		return -1;
	node* first = head;
	node *second = first->next;
	while(first && second && first->seq == (second->seq)-1){
		first = second;
		second = second->next;
	}
	if(second){
		return first->seq + 1;
	}
	else{
		return -1;
	}
}

void print_list(){
	node* tmp = head;
	while(tmp){
		printf("%d(%d)->", tmp->seq, tmp->size);
		tmp = tmp->next;
	}
	printf("\n");
}

bool isSeqPresent(int seq){
	node* tmp = head;
	while(tmp){
		if(tmp->seq == seq)
			return true;
		tmp = tmp->next;
	}
	return false;
}

