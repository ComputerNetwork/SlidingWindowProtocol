### File Transfer Over UDP
(*See readme.txt to configure, setup and run the program*)

##### Requirements

We are designing a File Transfer Protocol over UDP by implementing Sliding Window Protocol on
UDP that provides Reliable, In-order packet delivery and supports out-of-band data transfer. Sender/uploader will 
be able to send any file to receiver that is waiting.

To provide Reliable, In-order packet delivery, we implement sliding window protocol with following ARQs
1. Stop-And-Wait
2. Go-Back-N
3. Selective Repeat

Both sender and receiver create a socket connection, they bind to their local address (or the machine where they reside). 
Both will receive the address and port as command line argument to connect to the other end. Sender will send INIT, DATA packets to 
the other end and receiver will send ACK to the other end using this connection.

##### Design & Datastructures

Application data contains the following:
1. UFTP_Header_t struct
2. Binary Data 

Packet Header:

```
//UDP File Transfer Protocol Header
typedef struct UFTP_Header_t{
	//Packet Type. (0: INIT, 1: ACK, 2: DATA)
	ptype type:16;
	//Mode - Denote protocol (Stop-And-Wait/Go-Back-N/Selective Repeat)
	int mode:16;
	//Sequence Number
	int seq:16;
	//Acknowledgement Number
	int ack:16;
	//Length of payload
	size_t len;	
}UFTP_Header_t;
```

Packet Types:
```
typedef enum ptype{
	INIT,
	DATA,
	ACK
}ptype;
```
Packet type can be one of the following.
1. INIT - First message from uploader/sender to receiver.
2. DATA - Packet from sender to receiver that contains data
3. ACK  - Packet that contains Acknowledgement.

##### Implementation

###### Protocol Initialization
Receiver will start first and wait for senders. When the sender comes up, it sends INIT message to the receiver.

Init message contains the *Filename* and *Length*.

Header will contain
1. Initial sequence Number
2. Type as INIT

On successful receive of the message, receiver will send an ACK. And the data transfer will follow. Sender will 
also insert the Initial Sequence number with the INIT message that is computed in random.

Mode of receiver will be adjusted based on the value given in INIT packet from sender.
In All the methods, Data transfer proceeds only if Initialization is successful otherwise, connectin is terminated.
```
S->R: INIT
R->S: ACK

S->R: DATA
R->S: ACK
.
.
.
```
###### Sender Side Processing & Data Transfer
Sender will start sending data once the initial ACK is successfully received. If there is any failure, The connection is terminated.
Data transfer will happen as below based on the sliding window protocol we use.
###### 1. Stop-And-Wait:
- Sender will send One DATA packet and wait for ACK. 
- If the ack is out-of-order or duplicate, it will retransmit the packet again.
- If there is a timeout, then the packet will be retransmitted. In this case, we maintain retryCount. Whenever retryCount reaches MAX_RETRYCOUNT, the connection is terminated.
- Otherwise, the next packet is built and sent over network.

###### 2. Go-Back-N:
- Sender will maintain a window of size MODE passed as command line argument.
- It will send MODE packets one by one and wait for cumulative ACK.
- If the ACK denotes Loss of packets/Out-of-order delivery, MODE number of packets in current window will be retransmitted.
- If there is timeout, retryCount is updated, packets are retransmitted (untill we reach MAX_RETRYCOUNT)
- Otherwise, Window is moved to the successful ACK and the data transfer continues from there.

###### 3. Selective Repeat
- Sender will transfer MODE number of packets and wait for NACK from receiver.
- Receiver also maintains buffer of size MODE. It sends NACK only for those missing packets.
- If sender receives NACK, it retransmits only the missing packet.
- If the sender doesn't receive NACK for the particular timeout, then it proceeds with next set of packets.

###### Receiver Side Processing & ACKs
Receiver will send ACK based on the ARQ we are using.
###### 1. Stop-And-Wait:
- If the DATA packet received is the same as we are expecting, ACK is sent as SEQ + 1.
- Contents in the DATA packet is written to file 
- If there is no DATA and the time expires, we update retryCount and wait again till it reaches MAX_RETRYCOUNT. After that connection is dropped.

###### 2. Go-Back-N:
- Receiver is similar but it receives MODE number of packets one by one.
- If any packet is lost/out-of-order/corrupt we send cumulative ACK that acknowledges seq number of successfully received packet so far + 1.
- Timeouts handled similar as above.

###### 3. Selective Repeat
- In this case, Reciever also maintains a buffer of size MODE. 
- It keeps track of the buffers in this window, It sends NACK for the missing packet. (only one at a time)
- And waits for the particular packet to arrive. If this timesout, NACK is sent again.
- Receiver moves the window after all packets in this window have arrived.

##### Tests
To test this implementation, I maintained a counter. Upon reaching a particular number, I intentionally dropped ACK/DATA. Wireshark output is reviewed.
Duplicate ACK/DATA is always ignored.
###### Stop-And-Wait:
1. Drop ACK & Delayed ACK: Programmatically delay/drop ACK
 - Sender retransmits the packet successfully.
2. Delayed Send/Drop from Sender: Programmatically delay/Drop data packet
 - Receiver updates RetryCount and waits for data(No ACK is sent since no DATA is received). Sender faces timeout waiting for ACK. And it successfully transmits/retransmits the data after timeout.

###### Go-Back-N:
1. Drop ACK & Delayed ACK: Programmatically delay/drop ACK
 - Sender retransmits all the packets in the window.
2. Delayed Send/Drop from Sender: Programmatically delay/Drop data packet
 - Same as above, but sender retransmits all the packets in the window.
3. Out-of-order packet:
 - Receiver acknowledges last successful packet. And window moves from there.

###### Selective Repeat:
1. Drop DATA: Packet is dropped. receiver waits for it and sends NACK for that packet. Once the NACK is received, Sender retransmits the particular packet alone.

