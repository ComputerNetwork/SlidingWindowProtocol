#ifndef SRS_BUFF_LIST_H_
#define SRS_BUFF_LIST_H_

typedef struct node{
	int seq;
	char data[BUFFSIZE];
	int size;
	struct node *next;
}node;

extern node *head;
extern node *tail;

void insert(int seq, char *data, int size);
void delete_node();
void print_list();
void empty_list();
#endif
