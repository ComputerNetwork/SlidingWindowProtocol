#include "common.h"
#include "srs_buffer_list.h"

//Socket FD.
static int sockfd;
static int sockfd2;

static int MODE;
static char PORT[PLEN];
static char HOSTNAME[HNLEN];
static char FILENAME[FNLEN];

char buff[BUFFSIZE];
#define RCVTO 1
#define MYHOST "localhost"
#define MYPORT "4860"
struct addrinfo *raddr;
struct sockaddr_storage *other;
socklen_t addrlen;

void *get_in_addr(struct sockaddr *sa)
{
	return &(((struct sockaddr_in*)sa)->sin_addr);
}
void usage(){
	printf("./SR_Sender -p 4859 -h localhost -f sample.txt -m 5\n");
	exit(EXIT_FAILURE);
}
int open_udp_socket(){

	struct addrinfo hints, *servinfo;
	struct addrinfo *p;
	int rv;

	
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET; 
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = AI_PASSIVE; 
	//Bind to local host. receiver connects to (MYHOST:MYPORT)
	if ((rv = getaddrinfo(MYHOST, MYPORT, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		exit(EXIT_FAILURE);
	}

	// loop through all the results and bind to the first we can
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype,
			p->ai_protocol)) == -1) {
				perror("snw_receiver: socket");
				continue;
		}
		if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			perror("SNW_receiver: bind");
			continue;
		}
		break;
	}

	if (p == NULL) {
		fprintf(stderr, "snw_receiver: failed to open socket\n");
		exit(EXIT_FAILURE);
	}
	freeaddrinfo(servinfo);
	
	//Populate receiver Addr into raddr
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET; 
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = AI_PASSIVE; 

	if ((rv = getaddrinfo(HOSTNAME, PORT, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		exit(EXIT_FAILURE);
	}

	// loop through all the results and connect to the first we can
	for(raddr = servinfo; raddr != NULL; raddr = raddr->ai_next) {
		if ((sockfd2 = socket(raddr->ai_family, raddr->ai_socktype,
								raddr->ai_protocol)) == -1) {
				perror("SNW_Sender: socket");
				continue;
		}
		break;
	}
	if(raddr == NULL){
		fprintf(stderr, "snw_receiver: failed to open socket\n");
		exit(EXIT_FAILURE);
	}

}

/*
 * Parses arg list and loads values
 * Args:
 *  argc    - Argument count
 *  argv    - Array of char array (Argument list)
 * Returns:
 *  None
 * */
void parse_and_validate_args(int argc, char **argv){
	int c;
    bool mFlag = false, pFlag = false, hFlag = false, fFlag = false;
    int port = 0;
    while((c = getopt(argc, argv, "p:f:h:m:")) != -1){
        switch(c){
            case 'm':
                MODE = strtol(optarg, NULL, 10);
                mFlag = true;
                break;
            case 'p':
                if(strlen(optarg) > PLEN){
                	printf("Port length too long\n");
                	exit(EXIT_FAILURE);
                }
                strcpy(PORT, optarg);
                port = strtol(optarg, NULL, 10);
                pFlag = true;
                break;
            case 'f':
                if(strlen(optarg) > FNLEN){
                	printf("FILENAME length too long\n");
                	exit(EXIT_FAILURE);
                }
                strcpy(FILENAME, optarg);
                fFlag = true;
                break;
            case 'h':
                if(strlen(optarg) > HNLEN){
                	printf("HOSTNAME length too long\n");
                	exit(EXIT_FAILURE);
                }
                strcpy(HOSTNAME, optarg);
                hFlag = true;
                break;
            default:  
                usage();
        }
    }
    if(!mFlag || MODE > MAX_MODE || MODE <= 0){
		printf("Positive MODE value is mandatory. Must be <= %d\n", MAX_MODE);
        usage();
    } 
    if(!pFlag || port <= 0 || port > MAX_PORTS){
		printf("PORT value is mandatory. Must be within 0-65535\n");
        usage();
    } 
    if(!fFlag || (strcmp(FILENAME, "") == 0)){
		printf("FILENAME is mandatory\n");
        usage();    	
    }
    if(!hFlag || (strcmp(HOSTNAME, "") == 0)){
		printf("HOSTNAME is mandatory\n");
        usage();    	
    }
}
void print_args(){
	printf("ARGS:\n\nMODE: %d\nPORT: %s\nFILE:%s\nHOST:%s\n",
			MODE, PORT, FILENAME, HOSTNAME);
}

int compose_data_packet(int fd, int seqNum){
	int numbytes = 0;
	char file_buff[BUFFSIZE];
	UFTP_Header_t header;
	header.type = DATA;
	header.mode = MODE;
	header.seq = seqNum;
	header.ack = 0;
	numbytes = sizeof(header);
	size_t len;

	len = read(fd, file_buff, BUFFSIZE-numbytes-1);
	if(len < 0){
		perror("SR_Sender: Read");
		exit(EXIT_FAILURE);
	}
	header.len = len;
	memcpy(buff, &header, numbytes);
	if(len == 0){
		printf("Len is 0\n");
		return numbytes;
	}
	memcpy(buff+numbytes, file_buff, len);
	numbytes += len;
	return numbytes;
}

int process_nack_packet(char *buff, int numbytes, int seqBase, int seqMax){
	UFTP_Header_t *header = (UFTP_Header_t *)buff;
	#ifdef DBG
	printf("ACK:>SN: %d ACK:%d MODE:%d Type:%d\n", \
			header->seq, header->ack, header->mode, header->type);
	#endif
	if(header->type != ACK || header->ack < seqBase || header->ack >= seqMax)
		return 0;
	return header->ack;
}

size_t get_file_length(){
	int fd = open(FILENAME, O_RDONLY);
	if(fd == -1){
		perror("SR_Sender: open");
		exit(EXIT_FAILURE);
	}
	size_t len = lseek(fd, 0, SEEK_END);
	if(len <= 0){
		perror("SR_Sender: lseek");
		exit(EXIT_FAILURE);
	}
	#ifdef DBG
	printf("SR_Sender: FileLength: %ld\n", len);
	#endif
	close(fd);
	return len;
}
void initialize_connection(size_t *f_len, int seqBase){
	size_t numbytes;
	struct timeval tv;
	tv.tv_sec = RCVTO;
	tv.tv_usec = 0;
	if(setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) == -1){
		perror("SR_Sender: settimeout");
		exit(EXIT_FAILURE);
	}
	memset(buff, 0, BUFFSIZE);
	int rc = 0;
	while(rc < MAX_RETRY_COUNT){
		//Compose INIT message
		*f_len = get_file_length();
		UFTP_Header_t header = {INIT, MODE, seqBase, 0, 0};
		memcpy(buff, &header, sizeof(header));	
		int n = snprintf(buff+sizeof(header), BUFFSIZE, "%s\t%ld",\
						 FILENAME, *f_len);
		if(n <= 0){
			perror("SR_Sender: snprintf");
			exit(EXIT_FAILURE);
		}
		if ((numbytes = sendto(sockfd2, buff, n+sizeof(header), 0,
									raddr->ai_addr, raddr->ai_addrlen)) == -1) {
			perror("SR_Sender: sendto1");
			exit(1);
		}
		memset(buff, 0, BUFFSIZE);
		addrlen = sizeof(struct sockaddr_storage);
		if ((numbytes = recvfrom(sockfd, buff, BUFFSIZE-1 , 0,
				(struct sockaddr *)other, &addrlen)) == -1) {
				if(errno == EAGAIN || errno == EWOULDBLOCK){
					break;
				}								
				perror("recvfrom");
				exit(1);
		}
		rc++;
		printf("Retrying...%d\n", rc);
		continue;					
	}
	if(rc >= MAX_RETRY_COUNT){
		printf("Max retries Exceeded\n");
		exit(EXIT_FAILURE);
	}
}

void handle_receiver(){
	int numbytes, rv, dataLen;	
	char s[INET_ADDRSTRLEN];
	time_t t;
	//INITIALIZE seq Number
	srand((unsigned) time(&t));
	int seqBase = rand()%100;
	int seqMax = seqBase + MODE;
	off_t offset = 0;
	size_t f_len = 0;
	
	initialize_connection(&f_len, seqBase);
	seqBase++;
	seqMax = seqBase + MODE;
	int retryCount = 0;
	bool isRetry = false;
	int ii = 0;
	int fd = open(FILENAME, O_RDONLY);
	if(!fd){
		perror("SR_Sender: Open");
		exit(EXIT_FAILURE);
	}
	int missing_seq = 0;
	while(retryCount <= MAX_RETRY_COUNT && f_len > offset){
		if(!isRetry){
			//COMPOSE DATA message
			int si = seqBase;
			while(si < seqMax && f_len > offset){
				dataLen = compose_data_packet(fd, si);
				if(dataLen <= 0){
					printf("Error composing Data packet\n");
					exit(EXIT_FAILURE);
				}		
				insert(si, buff, dataLen);
				offset += (dataLen - sizeof(UFTP_Header_t));		
				si++;
				if ((rv = sendto(sockfd2, buff, dataLen, 0,
							raddr->ai_addr, raddr->ai_addrlen)) == -1) {
					perror("SR_Sender: sendto2");
					exit(1);
				}				
				memset(buff, 0, BUFFSIZE);
			}
		}
		else{
			node* tmp = head;
			while(tmp){
				if(tmp->seq == missing_seq){
					if ((rv = sendto(sockfd2, tmp->data, tmp->size, 0,
									raddr->ai_addr, raddr->ai_addrlen)) == -1) {
						perror("SR_Sender: sendto3");
						exit(1);
					}								
					break;
				}
				tmp = tmp->next;
			}
		}
		#ifdef DBG
		print_list();
		#endif
		//RECV NACK
		addrlen = sizeof(struct sockaddr_storage);
		if ((numbytes = recvfrom(sockfd, buff, BUFFSIZE-1 , 0,
				(struct sockaddr *)other, &addrlen)) == -1) {
				if(errno == EAGAIN || errno == EWOULDBLOCK){
					//No Nack. Successful transfer
					isRetry = false;
					retryCount = 0;
					empty_list();
					seqBase = seqMax;
					seqMax = seqBase + MODE;
					continue;					
				}								
				perror("recvfrom");
				exit(1);
		}
		
		missing_seq = process_nack_packet(buff, numbytes, seqBase, seqMax);
		if(missing_seq != 0){
			isRetry = true;
			retryCount++;	
			#ifdef DBG
			printf("Got NACK %d\n", missing_seq);
			#endif
		}
		else{
			#ifdef DBG
			printf("SR_Sender: Duplicate NACK\n");
			#endif
			isRetry = false;
			retryCount = 0;
			continue;
		}
		memset(buff, 0, BUFFSIZE);
	}
	if(retryCount >= MAX_RETRY_COUNT){
		printf("Retry Count exceeded. Terminating Connection...\n");
		exit(EXIT_FAILURE);
	}
	else{
		printf("SR_Sender: File Transfer Completed\n");
	}
	close(fd);

}

int main(int argc, char **argv)
{

	parse_and_validate_args(argc, argv);
	#ifdef DBG
	print_args();
	#endif
	open_udp_socket();
	
	handle_receiver();
	close(sockfd);
	close(sockfd2);
	return 0;
}
